# rust-chariott Container

Container images used to build rust-chariott

## Building the Container

Build the chariott container:
```bash
podman build -t localhost/chariott:latest .
```
  
By default the image will built with a frozen static hash commit. However is possible to build
with other version by specifying the `VERSION` variable during the build, e.g:  

```bash
podman build --build-arg CHARIOTT_VERSION=main -t localhost/chariott:latest .
```
_The above command will build the container using the `main` branch from the [chariott github project.](https://github.com/eclipse-chariott/chariott)_

## License

[MIT](./LICENSE)
