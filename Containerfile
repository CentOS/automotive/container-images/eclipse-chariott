ARG IMAGE_NAME=registry.fedoraproject.org/fedora
ARG IMAGE_TAG=40

FROM ${IMAGE_NAME}:${IMAGE_TAG} AS builder

ARG GIT_REPO_URL=https://github.com/eclipse-chariott/chariott.git
ARG GIT_REPO_REFS=main
ARG BUILD_TYPE=release

RUN dnf install -y git rust cargo unzip cmake protobuf-devel openssl-devel

RUN cargo install cargo-tarpaulin

RUN git clone \
-b ${GIT_REPO_REFS} \
${GIT_REPO_URL} \
/tmp/chariott

WORKDIR /tmp/chariott

RUN cargo build --${BUILD_TYPE} -p service_discovery

# TODO: fix build errors: https://gist.github.com/odra/f9989d44fa928c462618ba31b3114b0d
# RUN cargo build --${BUILD_TYPE} -p intent_brokering

RUN mkdir -p /tmp/chariott-build && \
mv /tmp/chariott/target/${BUILD_TYPE}/* /tmp/chariott-build/

FROM ${IMAGE_NAME}:${IMAGE_TAG}

# Copy our build
COPY --from=builder /tmp/chariott-build/service_discovery /usr/local/bin/chariott-service-discovery
